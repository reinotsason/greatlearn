from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for
from gl.db import get_db

bp = Blueprint('players', __name__, url_prefix='/players')

@bp.route('/')
def players():
    db = get_db()

    player_list = db.execute('SELECT name FROM Person').fetchall()

    return render_template('players/all.html', player_list=player_list)


def get_player_matches(db, player_id, league_id):
    completed = db.execute("""
        SELECT
        MD.day AS day,
        R.player_pt AS pt,
        R.player_mp AS mp,
        R.player_correct AS correct,
        Opp.name AS opp_name,
        OppR.player_mp AS opp_mp,
        OppR.player_correct AS opp_correct,
        M.id AS match_id
        FROM MatchDay MD
        INNER JOIN Match M ON MD.id = M.md_id
        INNER JOIN MatchResult R ON M.id = R.match_id
        INNER JOIN MatchResult OppR
        ON (M.id = OppR.match_id AND R.player_id <> OppR.player_id)
        INNER JOIN Person Opp ON OppR.player_id = Opp.id
        WHERE MD.league_id = :league_id
        AND R.player_id = :player_id
        ORDER BY day ASC
        """, {'league_id': league_id, 'player_id': player_id}).fetchall()

    live = db.execute("""
        SELECT
        MD.day AS day,
        Opp.name AS opp_name
        FROM MatchDay MD
        INNER JOIN LeaguePlayer LP ON MD.league_id = LP.league_id
        INNER JOIN Match M
        ON MD.id = M.md_id AND (LP.player_id = M.player1_id OR LP.player_id = M.player2_id)
        INNER JOIN Person Opp
        ON (Opp.id = M.player1_id OR Opp.id = M.player2_id) AND Opp.id <> LP.player_id
        WHERE MD.status = 1
        AND MD.league_id = :league_id
        AND LP.player_id = :player_id
        ORDER BY day ASC
        """, {'league_id': league_id, 'player_id': player_id}).fetchall()

    tbd = db.execute("""
        SELECT
        MD.day AS day,
        Opp.name AS opp_name
        FROM MatchDay MD
        INNER JOIN LeaguePlayer LP ON MD.league_id = LP.league_id
        INNER JOIN Match M
        ON MD.id = M.md_id AND (LP.player_id = M.player1_id OR LP.player_id = M.player2_id)
        INNER JOIN Person Opp
        ON (Opp.id = M.player1_id OR Opp.id = M.player2_id) AND Opp.id <> LP.player_id
        WHERE MD.status = 0
        AND MD.league_id = :league_id
        AND LP.player_id = :player_id
        ORDER BY day ASC
        """, {'league_id': league_id, 'player_id': player_id}).fetchall()

    return {'completed': completed, 'live': live, 'tbd': tbd}


def get_player_stats(db, player_id, league_id):
    category_stats = db.execute("""
        SELECT
        C.name AS category_name,
        SUM(S.correct) AS gets,
        AVG(S.correct) AS get_rate,
        COUNT(*) AS total
        FROM Question Q
        INNER JOIN Category C ON Q.category_id = C.id
        INNER JOIN MatchDay MD ON Q.md_id = MD.id
        INNER JOIN Submission S ON Q.id = S.question_id
        WHERE MD.league_id = :league_id
        AND MD.status = 2
        AND S.player_id = :player_id
        GROUP BY C.id
        ORDER BY get_rate DESC
        """, {'league_id': league_id, 'player_id': player_id}).fetchall()

    total_stats = db.execute("""
        SELECT
        SUM(S.correct) AS gets,
        AVG(S.correct) AS get_rate,
        COUNT(*) AS total
        FROM Question Q
        INNER JOIN MatchDay MD ON Q.md_id = MD.id
        INNER JOIN Submission S ON Q.id = S.question_id
        WHERE MD.league_id = :league_id
        AND MD.status = 2
        AND S.player_id = :player_id
        GROUP BY S.player_id
        """, {'league_id': league_id, 'player_id': player_id}).fetchone()

    return {'category': category_stats, 'total': total_stats}


def get_player_leagues(db, player_id):
    league_ids = db.execute('SELECT league_id FROM LeaguePlayer WHERE player_id = ?', (player_id,)).fetchall()

    leagues = []
    for x in league_ids:
        league_info = {}
        league_info['name'] = db.execute(
                'SELECT name FROM League WHERE id = ?', (x['league_id'],)).fetchone()['name']
        league_info['matches'] = get_player_matches(db, player_id, x['league_id'])
        league_info['stats'] = get_player_stats(db, player_id, x['league_id'])
        leagues.append(league_info)

    return leagues


def get_commissioner_leagues(db, player_id):
    leagues = db.execute('SELECT id, name FROM League WHERE commissioner_id = ?', (player_id,)).fetchall()

    return leagues


@bp.route('/<player>')
def profile(player):
    db = get_db()
    player = db.execute('SELECT id, name FROM Person WHERE name = ?', (player,)).fetchone()

    if player is None:
        return render_template('players/player_not_found.html')

    player_leagues = get_player_leagues(db, player['id'])
    commissioner_leagues = get_commissioner_leagues(db, player['id'])

    return render_template('players/profile.html',
            player=player,
            player_leagues=player_leagues,
            commissioner_leagues=commissioner_leagues)
