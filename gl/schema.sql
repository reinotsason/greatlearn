DROP TABLE IF EXISTS Person;
CREATE TABLE Person (
	id INTEGER PRIMARY KEY,
	name TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL
);

DROP TABLE IF EXISTS League;
CREATE TABLE League (
	id INTEGER PRIMARY KEY,
	name TEXT UNIQUE NOT NULL,
	commissioner_id INTEGER NOT NULL,
	FOREIGN KEY (commissioner_id) REFERENCES Person(id)
);

DROP TABLE IF EXISTS LeaguePlayer;
CREATE TABLE LeaguePlayer (
	league_id INTEGER,
	player_id INTEGER,
	FOREIGN KEY (league_id) REFERENCES League(id),
	FOREIGN KEY (player_id) REFERENCES Person(id)
);

DROP TABLE IF EXISTS MatchDay;
CREATE TABLE MatchDay (
	id INTEGER PRIMARY KEY,
	league_id INTEGER,
	day INTEGER,
	status INTEGER, -- 0 for future, 1 for active, 2 for past
	FOREIGN KEY (league_id) REFERENCES League(id)
);

DROP TABLE IF EXISTS Category;
CREATE TABLE Category (
	id INTEGER PRIMARY KEY,
	name TEXT
);

DROP TABLE IF EXISTS Question;
CREATE TABLE Question (
	id INTEGER PRIMARY KEY,
	md_id INTEGER NOT NULL,
	question_number INTEGER,
	question_text TEXT,
	answer_text TEXT,
	category_id INTEGER,
	UNIQUE (md_id, question_number),
	FOREIGN KEY (md_id) REFERENCES MatchDay(id),
	FOREIGN KEY (category_id) REFERENCES Category(id)
);

DROP TABLE IF EXISTS Submission;
CREATE TABLE Submission (
	id INTEGER PRIMARY KEY,
	player_id INTEGER,
	question_id INTEGER,
	answer TEXT,
	defence INTEGER,
	correct BOOLEAN,
	FOREIGN KEY (player_id) REFERENCES Person(id),
	FOREIGN KEY (question_id) REFERENCES Question(id)
);

DROP TABLE IF EXISTS Match;
CREATE TABLE Match (
	id INTEGER PRIMARY KEY,
	md_id INTEGER NOT NULL,
	player1_id INTEGER NOT NULL,
	player2_id INTEGER NOT NULL,
	FOREIGN KEY (md_id) REFERENCES MatchDay(id),
	FOREIGN KEY (player1_id) REFERENCES Person(id),
	FOREIGN KEY (player2_id) REFERENCES Person(id),
	UNIQUE (md_id, player1_id),
	UNIQUE (md_id, player2_id)
);

DROP TABLE IF EXISTS MatchResult;
CREATE TABLE MatchResult (
	match_id INTEGER,
	player_id INTEGER,
	player_mp INTEGER,
	player_correct INTEGER,
	-- 2 pts for a win, 1 for a tie, 0 for a loss, -1 for a forfeit
	player_pt INTEGER,
	FOREIGN KEY (match_id) REFERENCES Match(id),
	FOREIGN KEY (player_id) REFERENCES Person(id)
	-- TODO should probably have a constraint here that player_id matches one of the players in the match
);

