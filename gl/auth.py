import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for
from flask import current_app
import click
from werkzeug.security import check_password_hash, generate_password_hash
from gl.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

def get_person_by_name(db, name):
    result = db.execute(
            'SELECT id FROM Person WHERE name = ?',
            (name,)
            ).fetchone()
    return result


def add_person(db, name, password):
    hashed_password = generate_password_hash(password)
    db.execute(
            'INSERT INTO Person (name, password) VALUES (?, ?)',
            (name, hashed_password)
            )
    db.commit()


def update_password(db, person_id, new_password):
    hashed_password = generate_password_hash(new_password)
    db.execute(
            'UPDATE Person SET password = ? WHERE id = ?',
            (hashed_password, person_id)
            )
    db.commit()


@bp.cli.command('reset-password')
@click.argument('name')
def reset_password(name):
    db = get_db()
    person = get_person_by_name(db, name)
    update_password(db, person['id'], name)

    click.echo('Reset {}\'s password'.format(name))


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.person is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view


@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']

        db = get_db()
        error = None

        if not name:
            error = 'You gotta have a name!'
        elif not password:
            error = 'Hack3rz'
        elif get_person_by_name(db, name):
            error = 'There can be only one {}'.format(name)

        if error is None:
            add_person(db, name, password)
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')


@bp.route('/change_password', methods=('GET', 'POST'))
@login_required
def change_password():
    if request.method == 'POST':
        old_pw = request.form['old']
        new_pw = request.form['new']

        db = get_db()
        person = db.execute('SELECT * FROM Person WHERE id = ?', (g.person['id'],)).fetchone()
        error = None

        if not check_password_hash(person['password'], old_pw):
            error = 'Old password does not match'
        elif not new_pw:
            error = 'Need to specify a new password'

        if error is None:
            update_password(db, person['id'], new_pw)
            return redirect(url_for('index'))

        flash(error)

    return render_template('auth/change_password.html')


@bp.route('/login', methods=('GET','POST'))
def login():
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']

        db = get_db()
        error = None

        person = db.execute(
                'SELECT * FROM Person WHERE name = ?',
                (name,)
                ).fetchone()

        if person is None:
            error = 'No such person'
        elif not check_password_hash(person['password'], password):
            error = 'Incorrect password'

        if error is None:
            session.clear()
            session['person_id'] = person['id']
            return redirect(url_for('players.profile', player=name))

        flash(error)

    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_person():
    person_id = session.get('person_id')

    if person_id is None:
        g.person = None
    else:
        g.person = get_db().execute(
                'SELECT * FROM Person WHERE id = ?',
                (person_id,)
                ).fetchone()


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))
