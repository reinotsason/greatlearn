import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for
from werkzeug.exceptions import abort

from gl.db import get_db
from gl.auth import get_person_by_name
from itertools import repeat

bp = Blueprint('leagues', __name__, url_prefix='/leagues')

@bp.route('/')
def leagues():
    db = get_db()

    league_list = db.execute("""
            SELECT
            League.name AS name,
            Person.name AS commissioner
            FROM
            League LEFT OUTER JOIN Person
            ON
            League.commissioner_id = Person.id
            """
            ).fetchall()

    return render_template('leagues/all.html', league_list=league_list)


def get_league_by_name(db, name):
    result = db.execute(
            'SELECT * FROM League WHERE name = ?',
            (name,)
            ).fetchone()
    return result


def add_league(db, name, commissioner_id):
    db.execute(
            'INSERT INTO League (name, commissioner_id) VALUES (?, ?)',
            (name, commissioner_id)
            )
    db.commit()
    league = db.execute('SELECT * FROM League WHERE name = ?', (name,)).fetchone()

    return league


def add_match_days(db, league, num_days, num_questions=6):
    days = [(league['id'], i) for i in range(1, int(num_days) + 1)]
    db.executemany(
            'INSERT INTO MatchDay (league_id, day, status) VALUES (?,?,0)',
            days)
    match_days = db.execute(
            'SELECT id FROM MatchDay WHERE league_id = ?',
            (league['id'],)).fetchall()
    questions = [
            (match_day['id'], q_num)
            for q_num in range(1, int(num_questions) + 1)
            for match_day in match_days]
    db.executemany(
            'INSERT INTO Question (md_id, question_number) VALUES (?,?)',
            questions)

    db.commit()


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        league_name = request.form['name']
        num_days = request.form['num_days']

        db = get_db()
        error = None

        if not league_name:
            error = 'A league must have a name!'
        elif get_league_by_name(db, league_name):
            error = 'That league name is already taken'

        if error is None:
            league = add_league(db, league_name, g.person['id'])
            add_match_days(db, league, num_days)
            return redirect(url_for('leagues.leagues'))

        flash(error)

    return render_template('leagues/register.html')


def get_league_players(db, league_id):
    players = db.execute(
            'SELECT * FROM Person INNER JOIN LeaguePlayer ON Person.id = LeaguePlayer.player_id WHERE league_id = ?',
            (league_id,)).fetchall()
    return players


def get_match_days(db, league_id):
    match_days = db.execute(
            'SELECT * FROM MatchDay WHERE league_id = ?',
            (league_id,)).fetchall()
    return match_days


def get_league_stats(db, league_id):
    categories = db.execute("""
        SELECT DISTINCT
        C.id AS id,
        C.name AS name
        FROM MatchDay MD
        INNER JOIN Question Q ON MD.id = Q.md_id
        INNER JOIN Category C ON Q.category_id = C.id
        WHERE MD.league_id = ?
        """, (league_id,)).fetchall()

    category_stats = {}
    for cat in categories:
        stats = db.execute("""
            SELECT
            P.name AS name,
            SUM(S.correct) AS gets,
            AVG(S.correct) AS get_rate,
            COUNT(*) AS total
            FROM Question Q
            INNER JOIN Category C ON Q.category_id = C.id
            INNER JOIN MatchDay MD ON Q.md_id = MD.id
            INNER JOIN Submission S ON Q.id = S.question_id
            INNER JOIN Person P ON S.player_id = P.id
            WHERE MD.league_id = :league_id
            AND MD.status = 2
            AND C.id = :cat_id
            GROUP BY P.id
            ORDER BY get_rate DESC
            """, {'league_id': league_id, 'cat_id': cat['id']}).fetchall()
        category_stats[cat['name']] = stats

    return category_stats


@bp.route('/<league_name>')
def league(league_name):
    db = get_db()
    league = get_league_by_name(db, league_name)
    players = db.execute("""
    SELECT *,
    row_number()
    OVER (
        ORDER BY
        points DESC,
        match_points_differential DESC,
        total_match_points DESC
    ) AS rank,
    (pot_ufpa - ufpa) / (1.0 * pot_ufpa) AS defensive_efficiency,
    ufpe / (1.0 * pot_ufpe) AS offensive_efficiency
    FROM
    (
        SELECT
        P.name AS name,
        SUM(R.player_pt) AS points,
        SUM(R.player_mp) - SUM(OppR.player_mp) AS match_points_differential,
        SUM(
            CASE
            WHEN R.player_pt = 2 THEN 1
            ELSE 0
            END) AS wins,
        SUM(
            CASE
            WHEN R.player_pt = 1 THEN 1
            ELSE 0
            END) AS ties,
        SUM(
            CASE
            WHEN R.player_pt = 0 OR R.player_pt = -1 THEN 1
            ELSE 0
            END) AS losses,
        SUM(R.player_mp) AS total_match_points,
        SUM(R.player_correct) AS correct_answers,
        SUM(OppR.player_correct) AS correct_answers_allowed,
        SUM(
            CASE
            WHEN OppR.player_correct = 0 THEN 0
            WHEN OppR.player_correct = 1 THEN 3
            WHEN OppR.player_correct = 2 THEN 4
            WHEN OppR.player_correct = 3 THEN 5
            WHEN OppR.player_correct = 4 THEN 4
            WHEN OppR.player_correct = 5 THEN 3
            WHEN OppR.player_correct = 6 THEN 0
            END) AS pot_ufpa,
        SUM(
            CASE
            WHEN OppR.player_correct IS NULL THEN 0
            WHEN R.player_correct = 0 THEN 0
            WHEN R.player_correct = 1 THEN 3
            WHEN R.player_correct = 2 THEN 4
            WHEN R.player_correct = 3 THEN 5
            WHEN R.player_correct = 4 THEN 4
            WHEN R.player_correct = 5 THEN 3
            WHEN R.player_correct = 6 THEN 0
            END) AS pot_ufpe,
        SUM(
            CASE
            WHEN OppR.player_correct = 0 THEN 0
            WHEN OppR.player_correct = 1 THEN OppR.player_mp
            WHEN OppR.player_correct = 2 THEN OppR.player_mp - 1
            WHEN OppR.player_correct = 3 THEN OppR.player_mp - 2
            WHEN OppR.player_correct = 4 THEN OppR.player_mp - 4
            WHEN OppR.player_correct = 5 THEN OppR.player_mp - 6
            WHEN OppR.player_correct = 6 THEN 0
            END) AS ufpa,
        SUM(
            CASE
            WHEN OppR.player_correct IS NULL THEN 0
            WHEN R.player_correct = 0 THEN 0
            WHEN R.player_correct = 1 THEN R.player_mp
            WHEN R.player_correct = 2 THEN R.player_mp - 1
            WHEN R.player_correct = 3 THEN R.player_mp - 2
            WHEN R.player_correct = 4 THEN R.player_mp - 4
            WHEN R.player_correct = 5 THEN R.player_mp - 6
            WHEN R.player_correct = 6 THEN 0
            END) AS ufpe
        FROM LeaguePlayer LP
        INNER JOIN MatchResult R ON LP.player_id = R.player_id
        INNER JOIN Person P ON LP.player_id = P.id
        INNER JOIN Match M ON R.match_id = M.id
        INNER JOIN MatchDay MD ON M.md_id = MD.id
        INNER JOIN Person O
        ON ((O.id = M.player1_id OR O.id = M.player2_id) AND O.id <> P.id)
        INNER JOIN MatchResult OppR
        ON (OppR.match_id = R.match_id AND O.id = OppR.player_id)
        WHERE LP.league_id = ?
        GROUP BY P.id
    )
    ORDER BY rank
    """, (league['id'],)).fetchall()
    category_stats = get_league_stats(db, league['id'])
    return render_template('leagues/league_info.html', league=league, players=players, category_stats=category_stats)


def check_commissioner(league):
    if g.person['id'] != league['commissioner_id']:
        abort(403)


@bp.route('/<league_name>/commissioner')
def commissioner(league_name):
    db = get_db()
    league = get_league_by_name(db, league_name)
    check_commissioner(league)

    match_days = db.execute(
            'SELECT * FROM MatchDay WHERE league_id = ?',
            (league['id'],)).fetchall()
    return render_template('leagues/commissioner_page.html', league=league, match_days=match_days)


@bp.route('/<league_name>/add_players', methods=['GET', 'POST'])
def add_players(league_name):
    if request.method == 'POST':
        db = get_db()
        league = get_league_by_name(db, league_name)
        check_commissioner(league)
        name = request.form['name']
        db.execute(
                'INSERT INTO LeaguePlayer (league_id, player_id) SELECT ?, id FROM Person WHERE name = ?',
                (league['id'], name))
        db.commit()
        return redirect(url_for('leagues.add_players', league_name=league_name))
    return render_template('leagues/add_players.html', league_name=league_name)


@bp.route('/<league_name>/remove_players', methods=['GET', 'POST'])
def remove_players(league_name):
    if request.method == 'POST':
        db = get_db()
        league = get_league_by_name(db, league_name)
        check_commissioner(league)
        name = request.form['name']
        person = get_person_by_name(db, name)
        db.execute(
                'DELETE FROM LeaguePlayer WHERE player_id = ?',
                (person['id']))
        db.commit()
        return redirect(url_for('leagues.remove_players', league_name=league_name))
    return render_template('leagues/remove_players.html', league_name=league_name)


@bp.route('/<league_name>/generate_schedule', methods=['GET', 'POST'])
def generate_schedule(league_name):
    if request.method == 'POST':
        db = get_db()
        league = get_league_by_name(db, league_name)
        check_commissioner(league)
        players = get_league_players(db, league['id'])
        match_days = get_match_days(db, league['id'])
        if not players:
            return
        player_ids = [player['id'] for player in players]
        # TODO currently assuming even number of players!
        if len(players) % 2 == 0:
            player1_ids = player_ids[:len(players)//2]
            player2_ids = player_ids[len(players)//2:]
        else:
            return

        for match_day in match_days:
            # Remove existing matches
            db.execute('DELETE FROM Match WHERE md_id = ?', (match_day['id'],))
            match_ups = zip(repeat(match_day['id']), player1_ids, player2_ids)
            db.executemany(
                    'INSERT INTO Match (md_id, player1_id, player2_id) VALUES (?, ?, ?)',
                    match_ups)
            player1_ids.append(player2_ids.pop())
            player2_ids.insert(0, player1_ids.pop(1))
        db.commit()
        return redirect(url_for('leagues.commissioner', league_name=league_name))

    return render_template('leagues/generate_schedule.html', league_name=league_name)


def get_md(db, league_name, match_day):
    league = db.execute('SELECT * FROM League WHERE name = ?', (league_name,)).fetchone()
    md = db.execute("""
    SELECT
    L.id AS league_id,
    L.name AS league_name,
    L.commissioner_id as commissioner_id,
    M.id as id,
    M.day AS day,
    M.status AS status
    FROM MatchDay M
    INNER JOIN League L
    ON M.league_id = L.id
    WHERE league_name = ? AND day = ?
    """,
    (league_name, match_day)).fetchone()

    return md


def get_questions(db, md_id):
    questions = db.execute('SELECT * FROM Question WHERE md_id = ?', (md_id,))
    return questions


def get_submissions_for_marking(db, md_id):
    submissions = db.execute("""
    SELECT
    Q.question_number AS question_number,
    Q.question_text AS question,
    Q.answer_text AS correct_answer,
    S.answer AS given_answer,
    S.id AS id
    FROM Submission S
    INNER JOIN Question Q
    ON S.question_id = Q.id
    WHERE Q.md_id = ? AND S.correct IS NULL
    """, (md_id,)).fetchall()
    return submissions


def get_submissions(db, md_id):
    submissions = db.execute("""
    SELECT
    Q.question_number AS question_number,
    Q.question_text AS question,
    Q.answer_text AS correct_answer,
    S.answer AS given_answer,
    S.id AS id,
    S.correct AS correct,
    P.name AS player_name
    FROM Submission S
    INNER JOIN Question Q
    ON S.question_id = Q.id
    INNER JOIN Person P ON S.player_id = P.id
    WHERE Q.md_id = ?
    """, (md_id,)).fetchall()
    return submissions


def get_player_submission(db, md_id, player_id):
    submission = db.execute(
        """
        SELECT
        Q.question_number AS question_number,
        Q.question_text AS question_text,
        Q.answer_text AS correct_answer,
        S.answer AS given_answer,
        S.defence AS defence
        FROM
        Submission S
        INNER JOIN Question Q
        ON S.question_id = Q.id
        WHERE
        Q.md_id = ?
        AND
        S.player_id = ?
        """,
        (md_id, player_id)).fetchall()

    return submission


def get_outstanding_players(db, md_id):
    players = db.execute("""
    SELECT
    P.name AS name
    FROM Question Q
    INNER JOIN MatchDay MD
    ON Q.md_id = MD.id
    INNER JOIN LeaguePlayer LP
    ON MD.league_id = LP.league_id
    INNER JOIN Person P
    ON LP.player_id = P.id
    LEFT OUTER JOIN Submission S
    ON Q.id = S.question_id AND LP.player_id = S.player_id
    WHERE MD.id = ? AND S.id IS NULL
    GROUP BY name
    """, (md_id,)).fetchall()
    return players


def get_submitted_players(db, md_id):
    players = db.execute("""
    SELECT DISTINCT
    P.name AS name
    FROM Question Q
    INNER JOIN MatchDay MD
    ON Q.md_id = MD.id
    INNER JOIN LeaguePlayer LP
    ON MD.league_id = LP.league_id
    INNER JOIN Person P
    ON LP.player_id = P.id
    LEFT OUTER JOIN Submission S
    ON Q.id = S.question_id AND LP.player_id = S.player_id
    WHERE MD.id = ? AND S.id IS NOT NULL
    """, (md_id,)).fetchall()
    return players


def calculate_results(db, md_id):
    # Delete existing results for this match day
    db.execute("""
        DELETE FROM MatchResult
        WHERE match_id IN (SELECT id FROM Match WHERE md_id = ?)
        """, (md_id,))
    # And calculate the new ones
    db.execute("""
        INSERT INTO MatchResult (match_id, player_id, player_mp, player_correct, player_pt)
        SELECT
        raw.match_id, raw.player_id,
        CASE
        WHEN raw.opp_correct IS NULL THEN (
            CASE raw.correct
            WHEN 0 THEN 0
            WHEN 1 THEN 2
            WHEN 2 THEN 3
            WHEN 3 THEN 5
            WHEN 4 THEN 6
            WHEN 5 THEN 8
            WHEN 6 THEN 9
            ELSE 0
            END)
        ELSE raw.mp
        END AS player_mp,
        raw.correct,
        CASE
        WHEN raw.correct IS NULL THEN -1
        WHEN raw.opp_correct IS NULL THEN 2
        WHEN raw.mp > raw.opp_mp THEN 2
        WHEN raw.mp = raw.opp_mp THEN 1
        WHEN raw.mp < raw.opp_mp THEN 0
        END AS player_pt
        FROM (
            SELECT
            M.id AS match_id,
            LP.player_id AS player_id,
            SUM(
                CASE
                WHEN S.correct THEN OppS.defence
                ELSE 0
                END) AS mp,
            SUM(
                CASE
                WHEN OppS.correct THEN S.defence
                ELSE 0
                END) AS opp_mp,
            SUM(S.correct) AS correct,
            SUM(OppS.correct) AS opp_correct
            FROM LeaguePlayer LP
            INNER JOIN Match M
            ON (LP.player_id = M.player1_id OR LP.player_id = M.player2_id)
            INNER JOIN Question Q ON M.md_id = Q.md_id
            LEFT OUTER JOIN Submission S
            ON LP.player_id = S.player_id AND Q.id = S.question_id
            LEFT OUTER JOIN Submission OppS
            ON (OppS.player_id = M.player1_id OR OppS.player_id = M.player2_id)
            AND OppS.player_id <> LP.player_id
            AND S.question_id = OppS.question_id
            WHERE M.md_id = ?
            GROUP BY LP.player_id
        ) AS raw
        """, (md_id,))
    db.commit()


def get_match_day_info(db, md):
    match_results = db.execute("""
    SELECT
    P1.name AS player1_name,
    R1.player_mp AS player1_mp,
    R1.player_pt AS player1_pt,
    R1.player_correct AS player1_qs,
    P2.name AS player2_name,
    R2.player_mp AS player2_mp,
    R2.player_pt AS player2_pt,
    R2.player_correct AS player2_qs,
    M.id AS match_id
    FROM Match M
    INNER JOIN Person P1
    ON M.player1_id = P1.id
    INNER JOIN MatchResult R1
    ON
    M.id = R1.match_id
    AND
    P1.id = R1.player_id
    INNER JOIN Person P2
    ON M.player2_id = P2.id
    INNER JOIN MatchResult R2
    ON
    M.id = R2.match_id
    AND
    P2.id = R2.player_id
    WHERE M.md_id = ?
    """, (md['id'],)).fetchall()

    questions = db.execute("""
    SELECT
    Q.question_number AS number,
    Q.question_text AS text,
    Q.answer_text AS answer,
    AVG(S.correct) AS percent_correct,
    AVG(S.defence) AS avg_defence
    FROM Question Q
    INNER JOIN Submission S
    ON Q.id = S.question_id
    WHERE Q.md_id = ?
    GROUP BY question_number
    ORDER BY question_number ASC
    """, (md['id'],)).fetchall()

    info = {'results': match_results,
            'questions': questions}

    return info


@bp.route('/<league_name>/<match_day>')
def match_day(league_name, match_day):
    db = get_db()
    md = get_md(db, league_name, match_day)

    # If it's the commissioner, take them to the commissioner page
    if g.person['id'] == md['commissioner_id']:
        return redirect(url_for('leagues.match_day_commissioner',
            league_name=league_name, match_day=match_day))

    # Future match day, return a placeholder
    if md['status'] == 0:
        return render_template('leagues/match_day_placeholder.html')
    # If the match day is active, redirect to the submission page
    if md['status'] == 1:
        return redirect(url_for('leagues.submission', league_name=league_name, match_day=match_day))
    # If the match day is completed
    if md['status'] == 2:
        info = get_match_day_info(db, md)
        return render_template('leagues/match_day_info.html', info=info, md=md)


@bp.route('/add_category', methods=['GET', 'POST'])
def add_category():
    db = get_db()
    categories = db.execute('SELECT id, name FROM Category').fetchall()

    if request.method == 'POST':
        db.execute('INSERT INTO Category (name) VALUES (?)', (request.form['category_name'],))
        db.commit()
        return redirect(url_for('leagues.add_category'))

    return render_template('leagues/add_category.html', categories=categories)


@bp.route('/<league_name>/<match_day>/commissioner/questions', methods=['GET', 'POST'])
def edit_questions(league_name, match_day):
    db = get_db()
    md = get_md(db, league_name, match_day)
    league = get_league_by_name(db, league_name)
    check_commissioner(league)
    questions = get_questions(db, md['id'])
    categories = db.execute('SELECT id, name FROM Category').fetchall()

    if request.method == 'POST':
        for question in questions:
            q = 'question' + str(question['question_number'])
            a = 'answer' + str(question['question_number'])
            c = 'category' + str(question['question_number'])
            new_question_text = request.form[q]
            new_answer_text = request.form[a]
            new_category_id = request.form[c]
            db.execute("""
            UPDATE Question
            SET question_text = ?, answer_text = ?, category_id = ?
            WHERE id = ?
            """,
            (new_question_text, new_answer_text, new_category_id, question['id']))
        db.commit()

        return redirect(url_for('leagues.commissioner', league_name=league_name))

    return render_template('leagues/question_form.html', md=md, questions=questions, categories=categories)


@bp.route('/<league_name>/<match_day>/commissioner/all_submissions', methods=['GET', 'POST'])
def all_submissions(league_name, match_day):
    db = get_db()
    md = get_md(db, league_name, match_day)
    league = get_league_by_name(db, league_name)
    check_commissioner(league)

    submissions = get_submissions(db, md['id'])
    if request.method == 'POST':
        for submission in submissions:
            if request.form[str(submission['id'])] == 'yes':
                db.execute(
                        'UPDATE Submission SET correct = TRUE WHERE id = ?',
                        (submission['id'],))
                db.commit()
            elif request.form[str(submission['id'])] == 'no':
                db.execute(
                        'UPDATE Submission SET correct = FALSE WHERE id = ?',
                        (submission['id'],))
                db.commit()
            else:
                pass

        # If it's a match day that's already over, recalculate match results
        if md['status'] == 2:
            calculate_results(db, md['id'])

    return render_template('leagues/all_submissions.html', md=md, submissions=submissions)


# TODO Return errors for nonexistent match days
@bp.route('/<league_name>/<match_day>/commissioner', methods=['GET', 'POST'])
def match_day_commissioner(league_name, match_day):
    db = get_db()
    md = get_md(db, league_name, match_day)
    league = get_league_by_name(db, league_name)
    check_commissioner(league)
    questions = get_questions(db, md['id'])

    if md['status'] == 0:
        return redirect(url_for('leagues.edit_questions', league_name=league_name, match_day=match_day))

    if md['status'] == 1:
        submissions = get_submissions_for_marking(db, md['id'])
        outstanding_players = get_outstanding_players(db, md['id'])
        if request.method == 'POST':
            for submission in submissions:
                if request.form[str(submission['id'])] == 'yes':
                    db.execute(
                            'UPDATE Submission SET correct = TRUE WHERE id = ?',
                            (submission['id'],))
                    db.commit()
                elif request.form[str(submission['id'])] == 'no':
                    db.execute(
                            'UPDATE Submission SET correct = FALSE WHERE id = ?',
                            (submission['id'],))
                    db.commit()
                else:
                    pass
            return redirect(url_for('leagues.match_day_commissioner', league_name=league_name, match_day=match_day))

        return render_template('leagues/match_day_live_commissioner.html',
                md=md,
                submissions=submissions,
                outstanding_players=outstanding_players)

    if md['status'] == 2:
        info = get_match_day_info(db, md)
        return render_template('leagues/match_day_info.html', info=info, md=md)


@bp.route('/<league_name>/<match_day>/commissioner/status', methods=['GET', 'POST'])
def change_match_day_status(league_name, match_day):
    db = get_db()
    md = get_md(db, league_name, match_day)
    league = get_league_by_name(db, league_name)
    check_commissioner(league)

    if md['status'] == 0:
        if request.method == 'POST':
            db.execute('UPDATE MatchDay SET status = ? WHERE id = ?', (1, md['id']))
            db.commit()
            return redirect(url_for('leagues.match_day_commissioner', league_name=league_name, match_day=match_day))
        return render_template('leagues/match_day_status_form.html', md=md)

    if md['status'] == 1:
        if request.method == 'POST':
            db.execute('UPDATE MatchDay SET status = ? WHERE id = ?', (2, md['id']))
            # Now update results
            calculate_results(db, md['id'])

            return redirect(url_for('leagues.match_day_commissioner', league_name=league_name, match_day=match_day))
        return render_template('leagues/match_day_status_form.html', md=md)

    if md['status'] == 2:
        if request.method == 'POST':
            return redirect(url_for('leagues.match_day_commissioner', league_name=league_name, match_day=match_day))
        return render_template('leagues/match_day_status_form.html', md=md)


@bp.route('/<league_name>/<match_day>/submission', methods=['GET', 'POST'])
def submission(league_name, match_day):
    """ If the player hasn't submitted for an active match day, display the form.
    If the player has submitted, either for an active match day or a past one,
    show them their submission"""
    db = get_db()
    md = get_md(db, league_name, match_day)

    if md['status'] == 0:
        return render_template('leagues/matchday_placeholder.html')

    submission = get_player_submission(db, md['id'], g.person['id'])

    # Active match day
    if md['status'] == 1:
        # If the submission is empty, present the submission form
        if not submission:
            if request.method == 'POST':
                questions = get_questions(db, md['id'])
                defences = {0: 0, 1: 0, 2: 0, 3: 0}
                for question in questions:
                    answer = request.form['1' + str(question['question_number'])]
                    # Convert to float first, since '3.0' is accepted by the form validation,
                    # but can't be converted directly to an int
                    defence = int(float(request.form['2' + str(question['question_number'])]))
                    defences[defence] += 1
                    db.execute(
                        'INSERT INTO Submission (player_id, question_id, answer, defence) VALUES (?,?,?,?)',
                        (g.person['id'], question['id'], answer, defence))
                if defences[0] != 1 or defences[1] != 2 or defences[2] != 2 or defences[3] != 1:
                    error = "Invalid defence"
                    flash(error)
                    db.rollback()
                else:
                    db.commit()
                    return redirect(url_for('leagues.submission', league_name=league_name, match_day=match_day))

            questions = get_questions(db, md['id'])
            match_id = db.execute('SELECT id FROM Match WHERE player1_id = ? OR player2_id = ?', (g.person['id'], g.person['id'])).fetchone()
            opponent_id = db.execute("""
                SELECT CASE
                WHEN player1_id = :player THEN player2_id
                WHEN player2_id = :player THEN player1_id
                END AS id
                FROM Match
                WHERE
                (player1_id = :player OR player2_id = :player)
                AND
                md_id = :md_id
                """, {"player": g.person['id'], "md_id": md['id']}).fetchone()
            opponent = db.execute('SELECT * FROM Person WHERE id = ?', (opponent_id['id'],)).fetchone()
            return render_template('leagues/submission_form.html', league_name=league_name, match_day=match_day, questions=questions, opponent=opponent)

        submitted_players = get_submitted_players(db, md['id'])
        return render_template('leagues/submitted.html',
                md=md,
                submitted_players=submitted_players)

    # Past match day
    if md['status'] == 2:
        return redirect(url_for('leagues.match_day', league_name=league_name, match_day=match_day))


@bp.route('/<league_name>/<match_day>/submission/<player_name>')
def player_submission(league_name, match_day, player_name):
    db = get_db()
    md = get_md(db, league_name, match_day)
    player = get_person_by_name(db, player_name)

    submission = get_player_submission(db, md['id'], player['id'])
    req_submission = get_player_submission(db, md['id'], g.person['id'])

    # Don't allow players who haven't submitted to see submissions
    if md['status'] != 2 and not req_submission:
        return redirect(url_for('leagues.submission', league_name=league_name, match_day=match_day))

    if not submission:
        return render_template('leagues/not_submitted.html')

    return render_template('leagues/submission_completed.html', league_name=league_name,
            match_day=match_day,
            player_name=player_name,
            submission=submission)


@bp.route('/match/<match_id>')
def match(match_id):
    db = get_db()

    match_info = db.execute("""
        SELECT
        L.name AS league_name,
        MD.day AS match_day,
        P1.name AS player1_name,
        P2.name AS player2_name
        FROM Match M
        INNER JOIN MatchDay MD ON M.md_id = MD.id
        INNER JOIN League L ON MD.league_id = L.id
        INNER JOIN Person P1 ON M.player1_id = P1.id
        INNER JOIN Person P2 ON M.player2_id = P2.id
        WHERE M.id = ?
        """, (match_id,)).fetchone()

    submissions = db.execute("""
        SELECT
        Q.question_number AS question_number,
        Q.question_text AS question,
        Q.answer_text AS correct_answer,
        S1.answer AS player1_answer,
        S2.answer AS player2_answer,
        S1.defence AS player1_defence,
        S2.defence AS player2_defence,
        S1.correct AS player1_correct,
        S2.correct AS player2_correct
        FROM Question Q
        INNER JOIN Match M ON Q.md_id = M.md_id
        LEFT OUTER JOIN Submission S1 ON Q.id = S1.question_id AND M.player1_id = S1.player_id
        LEFT OUTER JOIN Submission S2 ON Q.id = S2.question_id AND M.player2_id = S2.player_id
        WHERE M.id = ?
        ORDER BY question_number ASC
        """, (match_id,)).fetchall()

    return render_template('leagues/match.html', match_info=match_info, submissions=submissions)
